package ui;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.*;

import drawables.Drawable;
import drawables.Point;
import drawables.NPolygon;
import utils.Clip;
import utils.Renderer;


public class PgrfFrame extends JFrame implements MouseMotionListener {

    static int FPS = 1000/30;
    private BufferedImage img;
    static int width = 800;
    static int height = 600;
    private JPanel panel, panel2;
    private JButton seedFill, seedFillPattern, drawing, clipping;
    private JLabel label;
    private Renderer renderer;
    private List<Drawable> drawables;
    private List<Point> pointPoly = new ArrayList<>();
    private List<Point> inPoints = new ArrayList<>();
    private NPolygon poly;
    private Clip clip;
    private Drawable drawable;
    private boolean fillMode = false;
    private int fillType;
    public static void main(String... args) {
        PgrfFrame pgrfFrame = new PgrfFrame();
        pgrfFrame.img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        pgrfFrame.init(width, height);
    }

    private void init(int width, int height) {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
        setSize(width, height);
        setTitle("Úloha 2 - Kristýna Hnízdilová");
        drawables = new ArrayList<>();
        panel = new JPanel();
        panel2 = new JPanel();
        panel2.setLayout(new BoxLayout(panel2, BoxLayout.X_AXIS));
        panel2.setBackground(Color.white);
        add(panel);
        add(panel2, BorderLayout.NORTH);
        seedFill = new JButton("Seed Fill Classic");
        seedFillPattern = new JButton("Seed Fill Pattern");
        drawing = new JButton("Draw Polygon");
        clipping = new JButton("Clip and Scan Line");
        label = new JLabel();
        label.setText("Click to draw a second polygon, use buttons");
        seedFill.setBackground(Color.white);
        seedFillPattern.setBackground(Color.WHITE);
        drawing.setBackground(Color.WHITE);
        clipping.setBackground(Color.white);
        panel2.add(drawing);
        panel2.add(seedFill);
        panel2.add(seedFillPattern);
        panel2.add(clipping);
        panel2.add(label);

        pointPoly.add(new Point(600,100));
        pointPoly.add(new Point(400,500));
        pointPoly.add(new Point(600,400));
        poly = new NPolygon(pointPoly);
        drawables.add(poly);

        clip = new Clip(pointPoly);

       seedFill.addActionListener(e -> {
           fillMode = true;
           fillType = 1;

       });

       seedFillPattern.addActionListener(e -> {
           fillMode = true;
           fillType = 2;
       });

       drawing.addActionListener(e -> fillMode = false);

       clipping.addActionListener(e -> {
           fillMode = true;
           clip.clip(inPoints);
           renderer.scanLine(clip.outPoints(),Color.BLUE.getRGB());


       });


        panel.addMouseMotionListener(this);
        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (!fillMode){
                        if(e.getClickCount()!=2){
                            if (drawable == null){
                                drawable = new NPolygon();
                            }

                            if (drawable instanceof NPolygon){
                                inPoints.add(new Point(e.getX(), e.getY()));
                                ((NPolygon) drawable).addPoint(new Point(e.getX(), e.getY()));
                            }
                            else {
                                drawable = null;
                            }}
                        else {
                            finishPolygon();
                        }



                } else {
                        if(fillType == 1 )
                        {
                    renderer.seedFill(e.getX(), e.getY(),
                            img.getRGB(e.getX(), e.getY()),
                            Color.PINK.getRGB());
                    }else if(fillType == 2){
                            renderer.seedFillPattern(e.getX(), e.getY(),
                                    img.getRGB(e.getX(), e.getY()));
                        }
                }

                super.mouseClicked(e);
            }
        });
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_SPACE){
                    finishPolygon();
                }
                super.keyReleased(e);
            }
        });

        setLocationRelativeTo(null);
        renderer = new Renderer(img);

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                draw();
            }
        }, 100, FPS);

        draw();
    }

    private void finishPolygon(){
        if (drawable != null){
            if (drawable instanceof NPolygon){
                ((NPolygon)drawable).setDone(true);
                drawables.add(drawable);
                drawable = null;
            }
        }
    }

    private void draw() {
        if (!fillMode){
            img.getGraphics().fillRect(0, 0, img.getWidth(), img.getHeight()); //clear
        }

        for (Drawable drawable : drawables) {
            drawable.draw(renderer);
        }
        if (drawable != null) {
            drawable.draw(renderer);
        }

        panel.getGraphics().drawImage(img, 0, 0, null);
        panel.paintComponents(getGraphics());
    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if (drawable != null) {
            drawable.modifyLastPoint(e.getX(), e.getY());
        }
    }


}





