package utils;


import drawables.Edge;
import drawables.Line;
import drawables.Point;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Renderer {

    private int color;
    private BufferedImage img;


    public Renderer(BufferedImage img) {
        this.img = img;
        color = Color.RED.getRGB();
    }

    private void drawPixel(int x, int y) {
        drawPixel(x, y, color);
    }

    private void drawPixel(int x, int y, int color) {
        if (x < 0 || x >= 800) return;
        if (y < 0 || y >= 600) return;
        img.setRGB(x, y, color);
    }

    public void lineDDA(int x1, int y1, int x2, int y2,int color) {

        float k, g, h; //G = PŘÍRŮSTEK X, H = PŘÍRŮSTEK Y
        int dy = y2 - y1;
        int dx = x2 - x1;
        k = dy / (float) dx;

        //určení řídící osy
        if (Math.abs(dx) > Math.abs(dy)) {
            g = 1;
            h = k;
            if (x1 > x2) { // prohození
                int temp = x1;
                x1 = x2;
                x2 = temp;
                temp = y1;
                y1 = y2;
                y2 = temp;
            }
        } else {
            g = 1 / k;
            h = 1;
            if (y1 > y2) { //otočení
                int temp = x1;
                x1 = x2;
                x2 = temp;
                temp = y1;
                y1 = y2;
                y2 = temp;
            }
        }

        float x = x1;
        float y = y1;
        int max = Math.max(Math.abs(dx), Math.abs(dy));

        for (int i = 0; i <= max; i++) {
            drawPixel(Math.round(x), Math.round(y),color);
            x += g;
            y += h;
        }
    }

    public void seedFill(int x, int y, int oldColor, int newColor) {
        if ((x >= 0) && (y >= 0) && (x < img.getWidth()) && (y < img.getHeight())){
        if (oldColor == img.getRGB(x, y)) {
            drawPixel(x, y, newColor);

            seedFill(x - 1, y, oldColor, newColor);
            seedFill(x + 1, y, oldColor, newColor);
            seedFill(x, y + 1, oldColor, newColor);
            seedFill(x, y - 1, oldColor, newColor);
        }
        }
    }

    public void seedFillPattern(int x, int y, int background){

        int [][]pattern =
                       {{1,0,0,0,1},
                        {0,1,0,1,0},
                        {0,0,1,0,0},
                        {0,1,0,1,0},
                        {1,0,0,0,1}, };
        int firstC = Color.magenta.getRGB();
        int secondC = Color.BLACK.getRGB();
        if ((x >= 0) && (y >= 0) && (x < img.getWidth()) && (y < img.getHeight())) {
            if ((img.getRGB(x, y) == background) && ((img.getRGB(x, y) != firstC || img.getRGB(x, y) != secondC))) {
                if (pattern[x % pattern.length][y % pattern.length] == 1) {
                    drawPixel(x, y, firstC);
                } else {
                    drawPixel(x, y, secondC);
                }

                seedFillPattern(x, y + 1, background);
                seedFillPattern(x, y - 1, background);
                seedFillPattern(x + 1, y, background);
                seedFillPattern(x - 1, y, background);
            }
        }
    }

    public void scanLine(List<Point> points, int fillColor){
        int yMax = points.get(0).getY();
        int yMin = points.get(0).getX();
        List<Edge> edges = new ArrayList<>();
        List<Integer> intersections = new ArrayList<>();
        Edge edge;
        for (int i = 0; i <points.size() ; i++) {
            if(yMin > points.get(i).getY())
            {
                yMin = points.get(i).getY();
            }
            if(yMax < points.get(i).getY())
            {
                yMax = points.get(i).getY();
            }
            edge = new Edge(points.get(i),points.get((i+1) % points.size()));
            if(!edge.isHorizontal()){
                edge.order();edge.compute();edges.add(edge);
            }
        }

        for (int y = yMin; y <= yMax; y++) {
            for(Edge e : edges){
                if(e.isIntersection(y)){
                    intersections.add(e.findX(y));
                }
            }
            Collections.sort(intersections);
            for(int i = 0;i < intersections.size();i+=2){
                lineDDA(intersections.get(i),y,intersections.get(i+1),y,fillColor);
            }
            intersections.clear();
        }
    }
}
