package utils;

import drawables.Edge;
import drawables.Point;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class Clip {

    private List<Point> area;
    private List<Point> out = new ArrayList<>();

    public Clip(List<Point> clA){
        this.area = clA;
    }


    public List<Point> clip(List<Point> inPoints) {
        for (int i = 0; i < area.size(); i++) {
            Edge edge1 = new Edge(area.get(i), area.get((i + 1)%area.size()));
            Point p1 = inPoints.get(inPoints.size()- 1);
            for (Point p2: inPoints) {
                if (edge1.isIn(p2)) {
                    if (!edge1.isIn(p1)) {
                        out.add(edge1.getInter(p1, p2));
                    }
                    out.add(p2);
                } else {
                    if (edge1.isIn(p1)) {
                        out.add(edge1.getInter(p1, p2));
                    }
                }
                p1 = p2;
            }
            inPoints.clear();
            inPoints.addAll(out);

        }
        return out;
    }

    public List<Point> outPoints(){
        return out;
    }
}
