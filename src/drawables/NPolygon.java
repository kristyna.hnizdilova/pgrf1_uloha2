package drawables;

import utils.Renderer;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class NPolygon implements Drawable{

    private List<Point> points;

    public void setDone(boolean done) {
        this.done = done;
    }

    private boolean done;

    public NPolygon() {
        points = new ArrayList<>();
    }

    public NPolygon(List<Point> clPoints) {
        this.points = clPoints;
    }

    public void addPoint(Point p) {
        points.add(p);
    }

    @Override
    public void draw(Renderer renderer) {
        if(!done){
            if (points.size() > 1) {
                for (int i = 0; i < points.size(); i++) {
                    Point point1 = points.get(i);
                    Point point2 = points.get((i + 1) % points.size());
                    renderer.lineDDA(point1.getX(), point1.getY(),
                            point2.getX(), point2.getY(),getColor());
                }
            }}

    }

    @Override
    public void modifyLastPoint(int x, int y) {
        // ignored
    }

    @Override
    public int getColor() {
        return Color.BLACK.getRGB();
    }


}
