package drawables;

public class Edge {
    int x1, y1, x2, y2;
    float k, q;

    public Edge(Point p1, Point p2){
        this.x1 = p1.getX();
        this.y1 = p1.getY();
        this.x2 = p2.getX();
        this.y2 = p2.getY();

    }

    public boolean isHorizontal(){
        return (y1 == y2);
    }

    public void order(){
        if (y2 < y1) {
            int pom = x1;
            x1 = x2;
            x2 = pom;
            pom = y1;
            y1 = y2;
            y2 = pom;
        }
        //TODO seřadit dle y1 <y2
    }



    public void compute(){
        double dx = x2 - x1;
        double dy = y2 - y1;
        k = (float)(dx / dy);
        q = x1 - (k * y1);
    }

    public int findX(int y){
        return (int)(k * y + q);
    }

    public boolean isIntersection(int y){
        return (y >= y1 && y < y2);
    }

    //clip

    public Point getInter(Point c, Point d) {
        double x0;
        double y0;
        double x3 = c.getX();
        double y3 = c.getY();
        double x4 = d.getX();
        double y4 = d.getY();
        x0 = ((x1 * y2 - x2 * y1) * (x3 - x4) - (x3 * y4 - x4 * y3) * (x1 - x2))
                / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
        y0 = ((x1 * y2 - x2 * y1) * (y3 - y4) - (x3 * y4 - x4 * y3) * (y1 - y2))
                / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
        return new Point((int) x0, (int) y0);
    }

    public boolean isIn(Point c) {
        double x3 = c.getX();
        double y3 = c.getY();
        double inside = ((x2 - x1) * (y3 - y1)) - ((x3 - x1) * (y2 - y1));
        return (inside > 0);

    }



}
